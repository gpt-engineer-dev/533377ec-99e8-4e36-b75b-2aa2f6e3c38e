document.addEventListener('DOMContentLoaded', () => {
  // Implement smooth scrolling
  const navLinks = document.querySelectorAll('header a[href^="#"]');
  for (let link of navLinks) {
    link.addEventListener('click', function (e) {
      e.preventDefault();
      let target = document.querySelector(this.getAttribute('href'));
      target.scrollIntoView({ behavior: 'smooth' });
    });
  }

  // Implement dark mode toggle
  // This would require a toggle button in the HTML which is not included in the current scope.
});
